# Adding Luna Gateway SDK to project

### 1. Download

a) Inside **project** scope `build.gradle` file in `allProjects` -> `repositories` add path to the repository:

    maven {
            url "https://gitlab.com/luna-xio/luna-sdk-android-release/-/raw/main"
        }
    maven {
            url 'https://repo.eclipse.org/content/repositories/egit-releases/'
        }
    maven { 
            url 'https://github.com/team-ethernet/SenML_API/raw/mvn-repo' 
        }

b) Inside **module** scope `build.gradle` file add dependency:

    implementation 'com.lunanets:android-gateway-sdk:1.6.69'

c) Inside **module** scope `gradle.properties` add at bottom of file line:

	android.enableJetifier=true

d) **Sync Project with Gradle Files**

![](https://i.ibb.co/yQZBVZ1/Screenshot-2021-07-06-at-14-35-12.png)

### 2. Request all needed permissions (Bluetooth, Location, SCHEDULE_EXACT_ALARM*)

#### 2.1. Recommended solution

Extend one of the activities (preferably launcher activity) with `AppCompatActivity` wrapper called `LocationPermissionActivity` to handle asking the user for all needed permission and turning on the device GPS module automatically. Luna Gateway will be turned on automatically after permissions are granted.

![](https://i.ibb.co/YfWPbSR/Screenshot-2021-07-05-at-16-10-24.png)

You can ask the wrapper to recheck all the needed permissions anytime you want by calling `LocationPermissionActivity.recheckAllPermissions()` method.

#### 2.2. Manual solution

<details>
  <summary>Show</summary>

To request **"While using the app"** location permission manually follow the official tutorials:

- https://developer.android.com/training/permissions/requesting
- https://developer.android.com/training/location/permissions

On devices running on Android 12 or newer, all the bluetooth permissions have to be granted as well.
Official tutorial:
- https://developer.android.com/develop/connectivity/bluetooth/bt-permissions

Additionally, devices with Android 14 or newer require asking for SCHEDULE_EXACT_ALARM permission as well before turning on gateway for the first time - gateway cannot properly restart without this permission in case something goes wrong with it.

</details>

### 3. Initilising Luna services

Now you can initialise Luna Gateway services. It can be done automatically by SDK (recommended solution), or manually. Saving and restoring Gateway state between app instances can be handled by the SDK itself (solution provided in step 2.1 and/or 2.2), but if it is not something you need, gateway can be turned on manually with the use of `LunaGateway.getInstance([context: Context]).turnGatewayOn([context: Context])`, and turned off with `LunaGateway.getInstance([context: Context]).turnGatewayOff([context: Context])` (beware that both bluetooth and location permission have to be granted at this point). Current status of LunaGateway (on/off) can be checked and shown in the UI with the use of `LunaGateway.gwOnLiveData`

#### 3.1. Recommended solution

Extend Application class with `LunaGatewayApplication` to launch Luna Gateway SDK foreground service automatically on consecutive app starts if all required permissions are granted by the user. It will handle saving and restoring state of the `LunaGateway` out of the box. If the app is started for the first time, and `LocationPermissionActivity` extension is not used, bluetooth and location permissions have to be asked from the user first, after which services have to be initialised manually.

![](https://i.ibb.co/wYJG2r8/Screenshot-2021-07-05-at-16-05-18.png)

If the app doesn't have Application class yet, follow the [instructions](https://docs.rudderstack.com/stream-sources/rudderstack-sdk-integration-guides/rudderstack-android-sdk/add-an-application-class-to-you-android-application).

#### 3.2. Manual solution

<details>
  <summary>Show</summary>

Inside Application class `onCreate()` function get instance of service initialiser with `PermissionsAwareServicesInitializer.getInstance([application context])` and call `tryInitialisingServicesOnAppStart()` method. If permissions are granted it will automatically restore Gateway's state from last app instance.

</details>

### 4. Run the app

Check the logs for `Luna Gateway SDK has been started` message. Hurray, the setup is done!

![](https://i.ibb.co/Jn6pGPt/Screenshot-2021-07-07-at-16-51-46.png)

### 5. Debugging

You can debug messages (both received and send) managed by the Gateway SDK with the use of DebugMode. By default, SDK debug mode is turned off but you can switch between it's on and off states with the use of `LunaGateway.getInstance([context: Context]).setDebugMode([isDebug: Boolean])`. While the DebugMode is turned ON, SDK will be showing system notifications about received commands, their execution status, and remote commands responses sending status.


# Understanding LocationPermissionActivity wrapper
## Overview
`LocationPermissionActivity` class is just a wrapper that will make it as easy as possible to use our SDK in your application. You just have to extend your main activity with this class, and it will automatically handle asking user for needed permissions and switching GPS and Bluetooth modules on. Otherwise, it will behave like your standard android `AppCompatActivity`.
## Handling user decisions results
It is possible that while using our `LocationPermissionActivity` wrapper you will want to handle user decisions results yourself, for example show some additional message to the user after he declines granting proper permissions. You can do it easily with the help of our `OnPermissionsActivityResults` class:

![](https://i.ibb.co/ys7J6WY/Screenshot-2021-07-14-at-15-24-17.png)

This class is just a container for the functions that will be called after specific user decisions:
- Rejecting granting any or all the needed permissions
- Rejecting turning bluetooth module on
- Rejecting turning GPS module on
- Fulfilling all requirements needed for Luna Gateway to work properly

To use this functionality, inside your activity that extends `LocationPermissionActivity` you just have to override **onPermissionActivityResults** field with your own `OnPermissionsActivityResults` implementation:

![](https://i.ibb.co/K2KXcLb/Screenshot-2021-07-14-at-15-31-39.png)

You don't have to specify all the above methods, if you want to use just some of them, inside your implementation provide only those that you need to use. If methods called after rejecting the prompts to turn either bluetooth or location on won't be provided, there will be default persistent android Snackbar shown with message telling user that the Gateway will not work properly without the module that was just being rejected.

![](https://i.ibb.co/SmBvXMm/Screenshot-2021-07-14-at-15-27-27.png)
*Calling `showProperContent()` function after Luna Gateway starts working properly*
